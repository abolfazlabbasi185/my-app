package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class editProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String fullname=getIntent().getStringExtra("fullname");
        setContentView(R.layout.activity_edit_profile);

        EditText editText = findViewById(R.id.tv_editProfile);
        editText.setText(fullname);
        Button button = findViewById(R.id.btn_editProfile_done);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullname = editText.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("fullname",fullname);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });
    }
}